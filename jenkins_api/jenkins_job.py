from ast import Constant
from enum import Enum
from datetime import datetime
from platform import platform
from urllib import response
from requests.auth import HTTPBasicAuth
import requests
import json


class JenkinsJob:
    def __init__(self, payload):
        self.payload = payload

    @classmethod
    def from_body(cls, body):
        # json parse
        try:
            payload = json.loads(body)
        except:
            return {'code': Constants.CODE.JSON_PARSE_ERROR, 'msg': "not a valid json: \n%s" % body}
        print('[JenkinsJob][from_body]', '\npayload:', payload)

        return cls(payload)

    def check_prerequisite(self, particular_list =[], any_list =[]):
        # check field by every signle one in list
        for key in particular_list:
            if key not in self.payload:
                return {'code': Constants.CODE.MISSING_REQUIRED, 'msg': "%s is required field" % key}
        
        if len(any_list) == 0:
            return None
            
        # check field by these key at the Sametime
        if len([k for k,v in self.payload.items() if k in any_list]) == 0:
            return {'code': Constants.CODE.BOTH_NONE_VERSION, 'msg': 'None of any version determinated'}
        
        return None
    
    def setup_job_config(self):
        self.job_config = JobConfig.prepare_by_version_keys(self.payload.get('project_name'), self.payload)
    
    def setup_device_config(self):
        self.job_config = JobConfig.prepare_by_device_type(self.payload.get('project_name'), self.payload)
    
    def setup_rollback_config(self):
        self.job_config = JobConfig.prepare_rollback(self.payload.get('project_name'), self.payload)

    def jobs_to_go(self):
        return self.job_config.get_jobs()

    def check_job_available(self):
        for job_name in self.jobs_to_go():
            # 檢查 job 的 queueItem 資料
            endpoint = Constants.Endpoints.STATE_CURRENT % (Constants.HOST, job_name)
            try:
                json_response = requests.get(endpoint, auth= Constants.BASIC_AUTH).json()
            except:
                return {'code': Constants.CODE.JENKINS_FAIL, 'msg': 'Jenkins went wrong with: ' + job_name + 'endpoint: ' + endpoint}
            if json_response.get('inQueue'):
                return {'code': Constants.CODE.JOB_IN_QUEUE, 'msg': "Job: %s had a request in queue" % job_name}

            # 檢查 last build 的 building 狀態
            endpoint = Constants.Endpoints.STATE_LAST_BUILD % (Constants.HOST, job_name)
            try:
                json_response = requests.get(endpoint, auth= Constants.BASIC_AUTH).json()
            except:
                return {'code': Constants.CODE.JENKINS_FAIL, 'msg': 'Jenkins went wrong with: ' + job_name + 'endpoint: ' + endpoint}
            if json_response.get('building'):
                return {'code': Constants.CODE.JOB_RUNNING, 'msg': "Job: %s is building" % job_name}
        
        return None

    def check_job_building(self):
        result = {
            Constants.BUILDING_KEY: {DeviceMapping.iOS.name: False, DeviceMapping.Android.name: False},
            Constants.LOG_KEY: {}}
        
        for device in self.jobs_to_go():
            # 檢查 job 的 queueItem 資料
            endpoint = Constants.Endpoints.STATE_CURRENT % (Constants.HOST, device.job_name())
            try:
                json_response = requests.get(endpoint, auth= Constants.BASIC_AUTH).json()
            except:
                result[Constants.LOG_KEY][device.name] = {'code': Constants.CODE.JENKINS_FAIL, 'msg': 'Jenkins went wrong with: ' + device.job_name() + 'endpoint: ' + endpoint}
            if json_response.get('inQueue'):
                result[Constants.BUILDING_KEY][device.name] = True
                result[Constants.LOG_KEY][device.name] = {'code': Constants.CODE.JOB_IN_QUEUE, 'msg': "Job: %s had a request in queue" % device.job_name()}
                continue

            # 檢查 last build 的 building 狀態
            endpoint = Constants.Endpoints.STATE_LAST_BUILD % (Constants.HOST, device.job_name())
            try:
                json_response = requests.get(endpoint, auth= Constants.BASIC_AUTH).json()
            except:
                result[Constants.LOG_KEY][device.name] = {'code': Constants.CODE.JENKINS_FAIL, 'msg': 'Jenkins went wrong with: ' + device.job_name() + 'endpoint: ' + endpoint}
            if json_response.get('building'):
                result[Constants.BUILDING_KEY][device.name] = True
                result[Constants.LOG_KEY][device.name] = {'code': Constants.CODE.JOB_RUNNING, 'msg': "Job: %s is building" % device.job_name()}
                continue
            
        return result
        
    def launch_job(self):
        check_urls = []
        for job_name, params in self.jobs_to_go().items():
            parameters = params | Constants.EXTRA_ARGUMENTS | {'ci_lane': Constants.Launcher.LANE}
            endpoint = Constants.Endpoints.JOB % (Constants.HOST, job_name)
            print('[JenkinsJob][launch_job]', '\nendpoint:', endpoint, '\nwith:', parameters)
            response = requests.get(endpoint, auth= Constants.BASIC_AUTH, params= parameters)

            if bool(response.text):
                return {
                    'code': Constants.CODE.JENKINS_FAIL,
                    'msg': 'Jenkins went wrong with: ' + job_name + ' params:' + json.dumps(params)}

            headers = dict(response.headers)
            check_urls.append(headers['Location'] + 'api/json')
        return {
            'code': Constants.CODE.SUCCESS,
            'msg': "Start running job with project: %s" % self.job_config.name,
            'check_url': check_urls}
    
    def sync_job(self):
        check_urls = []
        for job_name, params in self.jobs_to_go().items():
            parameters = params | Constants.EXTRA_ARGUMENTS | {'ci_lane': Constants.Syncer.LANE, 'ci_deploy': False}
            endpoint = Constants.Endpoints.JOB % (Constants.HOST, job_name)
            print('[JenkinsJob][sync_job]', '\nendpoint:', endpoint, '\nwith:', parameters)
            response = requests.get(endpoint, auth= Constants.BASIC_AUTH, params= parameters)

            if bool(response.text):
                return {
                    'code': Constants.CODE.JENKINS_FAIL,
                    'msg': 'Jenkins went wrong with: ' + job_name + ' params:' + json.dumps(params)}

            headers = dict(response.headers)
            check_urls.append(headers['Location'] + 'api/json')
        return {
            'code': Constants.CODE.SUCCESS,
            'msg': "Start running job with project: %s" % self.job_config.name,
            'check_url': check_urls}
    
    def rollback_project(self):
        check_urls = []
        for job_name, params in self.jobs_to_go().items():
            parameters = params | Constants.EXTRA_ARGUMENTS \
                | {'ci_lane': Constants.Rollbacker.LANE} \
                | {'token' : job_name}
            endpoint = Constants.Endpoints.JOB % (Constants.HOST, Constants.Rollbacker.JOB_NAME)
            print('[JenkinsJob][rollback_project]', '\nendpoint:', endpoint, '\nwith:', parameters)
            response = requests.get(endpoint, auth=Constants.BASIC_AUTH, params= parameters)

            if bool(response.text):
                return {
                    'code': Constants.CODE.JENKINS_FAIL,
                    'msg': 'Jenkins went wrong with: ' + job_name + ' params:' + json.dumps(params)
                }
            headers = dict(response.headers)
            check_urls.append(headers.get('Location') + 'api/json')
        return {
            'code': Constants.CODE.SUCCESS,
            'msg': "Start rollback with project: %s" % self.job_config.name,
            'check_url': check_urls}


class JobConfig(Enum):
    CN = 'CN'
    VN = 'VN'
    SP = 'SP'
    LIVE  = 'LIVE'

    @classmethod
    def prepare_by_version_keys(cls, name, payload):
        name = name.upper()
        
        # 檢查 _test 判斷上傳到測試路徑
        if Constants.Deploy.DEV_PATTERN in name:
            name = name.split(Constants.Deploy.DEV_PATTERN)[0]
            deploy_dev = True
        else:
            deploy_dev = True # 暫時都先上到測試路徑deploy_dev = False

        if name not in cls.__dict__:
            print("[JobConfig][prepare_by_version_keys] Project not found %s" % name)
            return None
            
        ins = cls(name)
        ins.jobs = {}
        for version_key in [item.value for item in VersionMapping]:
            if version_key in payload and payload.get(version_key):
                job_name = VersionMapping.prepare_arguments(version_key, name).job_name()
                job = {}
                job['token'] = job_name
                job['ci_branch'] = "release/%s" % payload.get(version_key)
                job['app_ids'] = payload.get('app_ids') if payload.get('app_ids') else ''
                job['chat_user'] = 'appuser'
                job |= ins.group_argument(deploy_dev) # FTP 上傳路徑
                job |= ins.drive_argument(deploy_dev, job_name) # Drive 上傳路徑
                ins.jobs[job_name] = job
        return ins

    def group_argument(self, deploy_dev):
        return {
            Constants.Deploy.GROUP_KEY:
            Constants.Deploy.GROUP_DEV[self.name] if deploy_dev else Constants.Deploy.GROUP_PRODUCTION[self.name]
        }

    def drive_argument(self, deploy_dev, job_name):
        return {
            'ci_synology_sharing': True,
            'ci_synology_drive_path':
            Constants.Deploy.DRIVE_DEV.get(job_name, '') if deploy_dev else Constants.Deploy.DRIVE_PRODUCTION.get(job_name, '')
        }
        pass

    @classmethod
    def prepare_by_device_type(cls, name, payload):
        name = name.upper()
        if name not in cls.__dict__:
            print("[JobConfig][prepare_by_device_type] Project not found %s" % name)
            return None
        
        ins = cls(name)
        ins.jobs = DeviceMapping.generate_devices(DeviceMapping.ALL, name)
        return ins

    @classmethod
    def prepare_rollback(cls, name, payload):
        name = name.upper()
         # 檢查 _test 判斷上傳到測試路徑
        if Constants.Deploy.DEV_PATTERN in name:
            name = name.split(Constants.Deploy.DEV_PATTERN)[0]
            deploy_dev = True
        else:
            deploy_dev = True # 暫時都先上到測試路徑deploy_dev = False

        if name not in cls.__dict__:
            print("[JobConfig][prepare_rollback] Project not found %s" % name)
            return None

        ins = cls(name)
        ins.jobs = { Constants.Rollbacker.JOB_NAME: {
            'ci_project_code': name,
            'ci_device': str(payload.get('device_type')),
            'app_ids': payload.get('app_ids'),
            'chat_user': payload.get('chat_user', '')
        }}        
        ins.jobs[Constants.Rollbacker.JOB_NAME] |= ins.group_argument(deploy_dev)
        return ins

    def get_jobs(self):
        return self.jobs


class VersionMapping(Enum):
    iOS = 'ios_version'
    Android = 'android_version'

    def job_name(self):
        return "%s-%s" % (self.project_name, self.name)

    @classmethod
    def prepare_arguments(cls, name, project_name):
        ins = cls(name)
        ins.project_name = project_name
        return ins


class DeviceMapping(Enum):
    iOS = 1
    Android = 2
    ALL = 3

    @classmethod
    def generate_devices(cls, device_type, project_name):
        # 設定專案名
        DeviceMapping.iOS.project_name = project_name
        DeviceMapping.Android.project_name = project_name

        if device_type == DeviceMapping.iOS.value:
            return [DeviceMapping.iOS]
        if device_type == DeviceMapping.Android.value:
            return [DeviceMapping.Android]
        # device type 對不到時當做全都要    
        return [DeviceMapping.iOS, DeviceMapping.Android]

    def job_name(self):
        return "%s-%s" % (self.project_name, self.name)


class Constants:
    HOST = 'http://172.30.7.250:8080'
    BASIC_AUTH = HTTPBasicAuth('root', '117223c9d40b8af9785c25e1760a65dd4f')
    BUILDING_KEY = 'building'
    LOG_KEY = 'query_log'
    EXTRA_ARGUMENTS = {}
    class Endpoints:
        JOB = "%s/jenkins/job/%s/buildWithParameters"
        STATE_CURRENT = "%s/jenkins/job/%s/api/json?pretty=true"
        STATE_LAST_BUILD  = "%s/jenkins/job/%s/lastBuild/api/json?pretty=true"
    class Launcher:
        LANE = "django"
    class Syncer:
        LANE = "django_syncer"
    class Rollbacker:
        JOB_NAME = "DjangoRollbacker"
        LANE = "django_rollback"
        GROUP_MAPPING = {
            'CN': 'native_fastlane',
            'VN': 'vnnative_fastlane',
            'SP': 'sportnative_fastlane',
            'LIVE': 'vnlive_fastlane',
        }
    class Deploy:
        DEV_PATTERN = '_TEST'
        # 上傳到 FTP
        GROUP_KEY = 'ci_group'
        GROUP_DEV = { 
            'CN': 'native_fastlane',
            'VN': 'vnnative_fastlane',
            'SP': 'sportnative_fastlane',
            'LIVE': 'vnlive_fastlane',
        }
        GROUP_PRODUCTION = {
            'CN': 'native',
            'VN': 'vnnative',
            'SP': 'sportnative',
            'LIVE': 'vnlive',
        }
        # 上傳到 Synology Drive
        DRIVE_DEV = {
            'CN-iOS': 'fastlane/中文彩/iOS 安裝包',
            'CN-Android': 'fastlane/中文彩/Android 安裝包',
            'VN-iOS': 'fastlane/越南彩/iOS 安裝包',
            'VN-Android': 'fastlane/越南彩/Android 安裝包',
            'SP-iOS': '',
            'SP-Android': '',
            'LIVE-iOS': '',
            'LIVE-Android': '',
        }
        DRIVE_PRODUCTION = { 
            'CN-iOS': 'App 最新安裝包/中文彩/iOS 安裝包',
            'CN-Android': 'App 最新安裝包/中文彩/Android 安裝包',
            'VN-iOS': 'App 最新安裝包/越南彩/iOS 安裝包',
            'VN-Android': 'App 最新安裝包/越南彩/Android 安裝包',
            'SP-iOS': '',
            'SP-Android': '',
            'LIVE-iOS': '',
            'LIVE-Android': '',           
        }
    class CODE:
        SUCCESS = 0
        JSON_PARSE_ERROR = 1001
        MISSING_REQUIRED = 1002
        BOTH_NONE_VERSION = 1003
        JOB_IN_QUEUE = 1004
        JOB_RUNNING = 1005
        JENKINS_FAIL = 1006