from ast import Constant
from asyncio import constants
from rest_framework.decorators import api_view
from requests.auth import HTTPBasicAuth
from django.http import JsonResponse
import json
import requests
from .jenkins_job import Constants, JenkinsJob
from django.views.decorators.csrf import csrf_exempt

"""
[Method]
GET                         測試用,不具功能
POST                        預設使用

[Field]
project_name(str)           required        專案名稱,現有 {cn, vn} 不分大小寫都可接受, 有後綴 '_test' 時, 檔案會上傳到測試路徑
ios_version(str)            depends         iOS 包版分支,將會組成 release/{ios_version} 去 git 上找分支來產出,不給時則 android_version 必填
android_version(str)        depends         Android 包版分支,將會組成 release/{android_version} 去 git 上找分支來產出,不給時則 ios_version 必填
app_ids(str)                optional        包版平台代號,多平台時逗號分隔,可接受中間任意空白
chat_user(str)              optional        打包完成後在頻道內 tag 的對象

[response]
success(bool)                               True 成功 False 失敗
msg(str)                                    執行結果
code(int)                                   0               成功
                                            1001            json 轉換失敗
                                            1002            缺少必填欄位
                                            1003            ios_version 及 android_version 同時都沒給
                                            1004            專案已有排入佇列的請求
                                            1005            專案處理中
                                            1006            Jenkins 系統異常
"""
@api_view(['GET', 'POST'])
def django_packer(request, format=None):
    print('request.path_info', request.path_info)
    print('request.body', request.body)
    print('request.POST', request.POST)
    # ---- GET ----
    if request.method == "GET":
        return JsonResponse({'msg': '[DjangoPacker] GET reponse'})
    # ---- POST ----
    job = JenkinsJob.from_body(request.body)
    if type(job) != JenkinsJob:
        result = job
        return JsonResponse(job | {'success': result['code'] == Constants.CODE.SUCCESS})

    result = job.check_prerequisite(['project_name'], ['ios_version', 'android_version'])
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    else:
        job.setup_job_config()

    result = job.check_job_available()
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    
    result = job.launch_job()
    return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})


"""
[Method]
GET                         測試用,不具功能
POST                        預設使用

[Field]
project_name(str)           required        專案名稱,現有 {cn, vn} 不分大小寫都可接受
ios_version(str)            depends         iOS 包版分支,將會組成 release/{ios_version} 去 git 上找分支來產出,不給時則 android_version 必填
android_version(str)        depends         Android 包版分支,將會組成 release/{android_version} 去 git 上找分支來產出,不給時則 ios_version 必填
chat_user(str)              optional        打包完成後在頻道內 tag 的對象

[response]
success(bool)                               True 成功 False 失敗
msg(str)                                    執行結果
code(int)                                   0               成功
                                            1001            json 轉換失敗
                                            1002            缺少必填欄位
                                            1003            ios_version 及 android_version 同時都沒給
                                            1004            專案已有排入佇列的請求
                                            1005            專案處理中
                                            1006            Jenkins 系統異常
"""
@api_view(['GET', 'POST'])
def django_syncer(request, format=None):
    print('request.path_info', request.path_info)
    print('request.body', request.body)
    print('request.POST', request.POST)
    # ---- GET ----
    if request.method == "GET":
        return JsonResponse({'msg': '[DjangoSyncer] GET reponse'})
    # ---- POST ----
    job = JenkinsJob.from_body(request.body)
    if type(job) != JenkinsJob:
        result = job
        return JsonResponse(job | {'success': result['code'] == Constants.CODE.SUCCESS})

    result = job.check_prerequisite(['project_name'], ['ios_version', 'android_version'])
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    else:
        job.setup_job_config()

    result = job.check_job_available()
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    
    result = job.sync_job()
    return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})


"""
[Method]
GET                         測試用,不具功能
POST                        預設使用

[Field]
project_name(str)           required        專案名稱,現有 {cn, vn} 不分大小寫都可接受
device_type(int)            required        裝置別,1: iOS 2: Android 3: ALL
app_ids(str)                required        包版平台代號,多平台時逗號分隔,可接受中間任意空白
                                            安全考量,給空白時不會變成全平台退版
chat_user(str)              optional        打包完成後在頻道內 tag 的對象

[response]
success(bool)                               True 成功 False 失敗
msg(str)                                    執行結果
code(int)                                   0               成功
                                            1001            json 轉換失敗
                                            1002            缺少必填欄位
                                            1004            專案已有排入佇列的請求
                                            1005            專案處理中
                                            1006            Jenkins 系統異常
"""
@api_view(['GET', 'POST'])
def django_rollbacker(request, format=None):
    print('request.path_info', request.path_info)
    print('request.body', request.body)
    print('request.POST', request.POST)
    # ---- GET ----
    if request.method == "GET":
        return JsonResponse({'msg': '[DjangoSyncer] GET reponse'})
    # ---- POST ----
    job = JenkinsJob.from_body(request.body)
    if type(job) != JenkinsJob:
        result = job
        return JsonResponse(job | {'success': result['code'] == Constants.CODE.SUCCESS})
    
    result = job.check_prerequisite(['project_name', 'device_type', 'app_ids'])
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    else:
        job.setup_rollback_config()

    result = job.check_job_available()
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})

    result = job.rollback_project()
    return JsonResponse(result| {'success': result['code'] == Constants.CODE.SUCCESS})


"""
[Method]
GET                     	測試用,不具功能
POST                    	預設使用

[Field]
project_name(str)       	required        專案名稱,現有 {cn, vn} 不分大小寫都可接受

[response]
success(bool)                           	True 成功 False 失敗
msg(str)                                	執行結果
code(int)                                   0               成功
                                            1001            json 轉換失敗
                                            1002            缺少必填欄位
building(object)                          	是否正在執行,邏輯是只有確定 Job 正在執行才會回 True,其他包含異常都回 False
    iOS(bool)                               iOS 專案包版狀態
    Android(bool)                           Android 專案包版狀態
query_log(object)                           記錄個別專案查詢結果
    iOS(obj)
        code(str)                           1004            專案已有排入佇列的請求
                                            1005            專案處理中
                                            1006            Jenkins 系統異常
        msg(str)                            查詢結果
    Android(obj)
        code(str)                           1004            專案已有排入佇列的請求
                                            1005            專案處理中
                                            1006            Jenkins 系統異常
        msg(str)                            查詢結果
"""
# @csrf_exempt
def job_state(request, format=None):
    # print('request.path_info', request.path_info)
    # print('request.body', request.body)
    # print('request.POST', request.POST)
    # ---- GET ----
    if request.method == "GET":
        return JsonResponse({'msg': '[JobState] GET reponse'})
    # ---- POST ----
    job = JenkinsJob.from_body(request.body)
    if type(job) != JenkinsJob:
        result = job
        return JsonResponse(job | {'success': result['code'] == Constants.CODE.SUCCESS})

    result = job.check_prerequisite(['project_name'])
    if result:
        return JsonResponse(result | {'success': result['code'] == Constants.CODE.SUCCESS})
    else:
        job.setup_device_config()

    result = job.check_job_building()
    return JsonResponse(result | 
    {
        'successs': True,
        'code': Constants.CODE.SUCCESS,
        'msg': '',
    })