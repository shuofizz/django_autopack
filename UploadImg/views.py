import os

import requests
from django.contrib import messages
from django.shortcuts import render, redirect, HttpResponse
from .forms import *
from django.core.files.storage import FileSystemStorage
from PIL import Image

# Create your views here.
def show_test_index(requests):
    return render(requests, 'index.html')

def app_icon_view(request):
    if request.method == 'POST':
        form = IconForm(request.POST, request.FILES)
        if form.is_valid():
            # form.save()
            file = request.FILES['icon_img']
            fileName =  request.POST['name'] + f".{file.name.split('.')[-1] }"

            folder = f"media/images/{ request.POST['name'] }"
            fs = FileSystemStorage(location=folder)  # defaults to   MEDIA_ROOT

            img = Image.open(file)

            try:
                img.save(f"{folder}/{fileName}")
            except FileNotFoundError as err:
                os.mkdir(folder)
                img.save(f"{folder}/{fileName}")
            except:
                return HttpResponse('can not save.')

            return redirect('success')

    else:
        form = IconForm()

    return render(request, 'index.html', { 'form' : form })

def success(request):
    return HttpResponse('Success Uploaded!')

def upload(request):
    # messages.error(reqyest, 'QQ456')
    iconUploaded = True
    bgUploaded = True
    try:
        iconFile = Image.open(request.FILES['icon_img'])
    except:
        print('no icon')
        iconUploaded = False

    try:
        launchFile = Image.open(request.FILES['bg_img'])
    except:
        print('no bg')
        bgUploaded = False

    code = request.POST['upload']
    folder = f"media/images/{code}"
    try:
        iconFile.save(f"{folder}/icon.png")
    except FileNotFoundError:
        os.mkdir(folder)
        iconFile.save(f"{folder}/icon.png")
    except:
        print("can't read file.")

    try:
        launchFile.save(f"{folder}/launchscreen.png")
    except FileNotFoundError:
        os.mkdir(folder)
        launchFile.save(f"{folder}/launchscreen.png")
    except:
        print("can't read file.")

    if iconUploaded and bgUploaded:
        messages.info(request, f'{code}平台 Icon, 背景圖上傳成功.')
    elif iconUploaded == True and bgUploaded == False:
        messages.info(request, f'{code}平台 Icon上傳成功.')
    elif iconUploaded == False and bgUploaded == True:
        messages.info(request, f'{code}平台 背景圖上傳成功.')
    else:
        messages.error(request, f'未選擇任何圖片或上傳失敗')

    print('do upload\n', request.POST)