from django.db import models

# Create your models here.
class appIcon(models.Model):
    #blank參數: 略過檢查, default is False.
    name = models.CharField(blank=True,max_length=50, verbose_name='平台')
    icon_img = models.ImageField(blank=True, upload_to='images/', verbose_name='Icon')
    bg_img = models.ImageField(blank=True, upload_to='images/', verbose_name='啟動背景圖')
