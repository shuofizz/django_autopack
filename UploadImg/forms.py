from django import forms
from .models import *

class IconForm(forms.ModelForm):

    class Meta:
        model = appIcon
        fields = ('icon_img','bg_img',)
        # fields = '__all__'