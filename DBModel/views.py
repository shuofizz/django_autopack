from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.contrib.admin import helpers
from django.contrib import messages
from .models import PlatformsData, PlatformsData_Vi, PlatformsData_Sp
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
import requests
from django.http import FileResponse
from DBModel.version_choices import Choices

# Create your views here.
@login_required
def show_list(request):
    # print(request.user.is_superuser)
    # print(request.user.groups.all()[0].name)
    # print(request.user)
    try:
        keys = request.GET.keys()
        current_platform = next(iter(keys))
    except:
        current_platform = 'cn'

    print(current_platform)

    context = {}
    data_list = PlatformsData.objects.all().order_by('id')
    if current_platform == 'cn':
        data_list = PlatformsData.objects.all().order_by('id')
    elif current_platform == 'vi':
        data_list = PlatformsData_Vi.objects.all().order_by('id')
    elif current_platform == 'sp':
        data_list = PlatformsData_Sp.objects.all().order_by('id')

    # f = list(map(lambda  nn: nn.verbose_name, PlatformsData._meta.fields))
    column_header = [f.verbose_name for f in PlatformsData._meta.fields if f.name in ['code','ios_appname','android_appname','android_version','ios_version','ios_app_id']]
    print(column_header)

    platform_config = {'cn':'中文', 'vi':'越南', 'sp':'體育'}

    context['AppName'] = 'App 平台資訊'
    context['data_list'] = data_list
    context['header'] = column_header
    context['platforms'] = platform_config
    return render(request, 'list.html', context)

##打包
def packer(self, request, objects):
    #打包模式: 1:只包iOS, 2:只包android, 3:雙平台都包
    packerState = mobile_checkBox(request)

    if len(objects) > 0 and packerState != 0:
        ios_version = ""
        android_version = ""
        platforms = ""
        if packerState == 1:
            ios_version = request.POST['ios_version']
            objects.update(ios_version=ios_version)
        if packerState == 2:
            android_version = request.POST['android_version']
            objects.update(android_version=android_version)
        if packerState == 3:
            ios_version = request.POST['ios_version']
            android_version = request.POST['android_version']
            objects.update(android_version=android_version, ios_version=ios_version)

        for obj in objects:
            platforms += obj.code + ","
        platforms = platforms[:-1]

        print("ios:",ios_version, "android:", android_version)
        url = 'http://172.30.7.250/jenkins_api/django_packer/'

        r = requests.post(url, json={'project_name': 'cn',
                                     'ios_version': f'{ios_version}',
                                     'android_version': f'{android_version}',
                                     'app_ids': f'{platforms}',
                                     'chat_user': 'appuser'})
        print(r.json())
        messages.info(request, '送出打包請求')
    else:
        messages.error(request, '未選擇任何平台')

    return HttpResponseRedirect('../')

#通知jenkins更新config
def sync_jenkins(meta, ios_version, android_version):
    if meta == PlatformsData:
        project_name = 'cn'
    elif meta == PlatformsData_Vi:
        project_name = 'vn'
    else:
        return
    url = 'http://172.30.7.250/jenkins_api/django_syncer/'
    r = requests.post(url, json={'project_name': f'{project_name}',
                                 'ios_version': f'{ios_version}',
                                 'android_version': f'{android_version}'
                                 })
    print(r.json())


@csrf_exempt
def ajax_request(request):
    print('ajax request')
    return  HttpResponse('ajax response')

def get_platform_version(meta, device: int):
    #device  0:ios 1:android
    if meta == PlatformsData:
        if device == 0:
            return Choices().get_branches(68)
        else:
            return Choices().get_branches(70)

##檢查勾選要包的平台
def mobile_checkBox(request):
    state = 0
    if 'do-ios' in request.POST and 'do-android' in request.POST:
        state = 3
    elif 'do-android' in request.POST:
        state = 2
    elif 'do-ios' in request.POST:
        state = 1

    return state