from django.db import models

signs = (
    ('', '-'),
    ('企業簽', '企業簽'),
    ('代簽', '代簽'),
    ('超級簽', '超級簽'),
    ('代簽+超級簽', '代簽+超級簽'),
)

# Create your models here.
class PlatformsData(models.Model):
    code = models.CharField(max_length= 100)
    ios_appname = models.CharField(max_length= 100)
    android_appname = models.CharField(max_length= 100)
    download_url = models.TextField(verbose_name="域名")
    remark = models.CharField(choices=signs, default=signs[0], verbose_name="簽名方式", null=True, blank=True, max_length= 100)
    remark2 = models.TextField(verbose_name="----註解----",null=True, blank=True)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField(null=True, blank=True)

    class Meta:
        # managed = False
        verbose_name = '平台'
        verbose_name_plural = '中文版App'
        db_table = 'platforms_cn'
        permissions = (('can_edit_field', 'Can_edit_field'),)


class PlatformsData_Vi(models.Model):
    code = models.CharField(max_length=100)
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2")
    remark = models.TextField(verbose_name="註解", null=True, blank=True)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField()

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '越南版App'
        db_table = 'platforms_vn'


class PlatformsData_Sp(models.Model):
    code = models.CharField(max_length=100)
    ios_appname = models.CharField(max_length=100)
    android_appname = models.CharField(max_length=100)
    download_url = models.TextField(verbose_name="域名")
    download_url2 = models.TextField(verbose_name="域名2")
    remark = models.TextField(verbose_name="註解", null=True, blank=True)
    android_version = models.CharField(max_length=100)
    ios_version = models.CharField(max_length=100)
    android_app_id = models.TextField()
    ios_app_id = models.TextField()
    ios_master_secret = models.TextField()
    ios_app_key = models.TextField()
    android_master_secret = models.TextField()
    android_app_key = models.TextField()
    open_install_key = models.TextField()

    class Meta:
        verbose_name = '平台'
        verbose_name_plural = '體育版App'
        db_table = 'platforms_sp'