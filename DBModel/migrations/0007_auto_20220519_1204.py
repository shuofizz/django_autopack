# Generated by Django 3.1.14 on 2022-05-19 04:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('DBModel', '0006_auto_20220519_1139'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='platformsdata_sp',
            options={'permissions': (('can_edit_field_sp', 'Can_edit_field_sp'),), 'verbose_name': '平台', 'verbose_name_plural': '體育版App'},
        ),
    ]
