# Generated by Django 3.1.13 on 2022-05-25 03:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DBModel', '0008_auto_20220519_1518'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='platformsdata_vi',
            options={'verbose_name': '平台', 'verbose_name_plural': '越南版App'},
        ),
        migrations.AlterField(
            model_name='platformsdata',
            name='android_version',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='platformsdata',
            name='download_url',
            field=models.URLField(verbose_name='域名'),
        ),
    ]
