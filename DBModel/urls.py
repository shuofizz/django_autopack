from django.urls import path
from DBModel import views

urlpatterns = [
    path('my-ajax-test/', views.ajax_request, name='ajax-test-view'),
]