// 批次修改<Dialog>
var updateButton = document.getElementById('updateDetails');
var favDialog = document.getElementById('favDialog');
var confirmBtn = document.getElementById('confirmBtn');
var cancelButton = document.getElementById('close');
var form = document.getElementById('archive_form');
var platforms = document.getElementById('selected_platforms');

// "Update details" button opens the <dialog> modally
// 批次修改
/*
updateButton.addEventListener('click', function onOpen() {
    favDialog.show();
    selects = get_select();
    $('.p-versions').each(function (i, sec) {
       sec.innerHTML = selects;
    });
});

// "Confirm" button of form triggers "close" on dialog because of [method="dialog"]
favDialog.addEventListener('close', function onClose() {

});

confirmBtn.addEventListener('click', function () {
    form.onsubmit = function () {
        return true
    };
});

cancelButton.addEventListener('click', function () {
    favDialog.close();
    form.onsubmit = function () {
        return false
    };

    // $.ajax({
    //     type: "POST",
    //     url: 'my-ajax-test/',
    //     data: {},
    //     success: function (response) {
    //         console.log('cancel click');
    //     }
    // });
});
*/

//打包<Dialog>
var archiveDialog = document.getElementById('archive_dialog');
var archiveButton = document.getElementById('archive');
var archiveBtn = document.getElementById('archiveBtn');
var archive_cancel = document.getElementById('archive_dialog_close');
var csrf_token  = $.cookie('csrftoken');

archiveButton.addEventListener('click', function onOpen() {
    archiveDialog.show();
    selects = get_select();

    $('.p-versions').each(function (i, sec) {
       sec.innerHTML = selects;
    });

    get_jenkins_state();
    // update_version_td();
});

function get_select() {
    stt = "";
    try {
        $("#result_list").find("tr.selected").each(function (i, sec) {
            selectedCode = sec.childNodes[1].childNodes[0].text;
            stt += selectedCode + "<br>";
        });
    }catch (error) {}

    if (stt === "") {
        stt = "未選擇任何平台";
    }

    return stt;
}

function update_version_td() {
    var td_ios = $('#td-ios')[0];
    var td_android = $('#td-android')[0];
    try {
        tr_selecteds = $("#result_list").find("tr.selected");
        and_opts = tr_selecteds[0].getElementsByClassName('field-android_version')[0].childNodes[0];
        ios_opts = tr_selecteds[0].getElementsByClassName('field-ios_version')[0].childNodes[0];
        iv = ios_opts.options[ios_opts.selectedIndex].text;
        av = and_opts.options[and_opts.selectedIndex].text;
    }catch (e) {
        iv = "NA";
        av = "NA";
    }

    td_ios.innerText = iv;
    td_android.innerText = av;
}

function get_jenkins_state() {
    $.ajax(
        {
            type: 'POST',
            url: '/jenkins_api/job_state/',
            dataType: 'json',
            contentType: 'application/json',
            headers: {'X-CSRFToken': csrf_token},
            data: JSON.stringify({'project_name': 'cn'}),
            success: function (response) {
                console.log(response['building']);
                document.getElementById('ios').disabled = false;

                var ios_building = response['building']['iOS'];
                var android_building = response['building']['Android'];
                document.getElementById('ios').disabled = ios_building;
                document.getElementById('android').disabled = android_building;
                document.getElementById('ios-label-text').innerText = 'iOS';
                document.getElementById('android-label-text').innerText = 'Android';

                if (ios_building == true) {
                    document.getElementById('ios-label-text').innerText = 'iOS(打包中...)';
                }
                if (android_building == true) {
                    document.getElementById('android-label-text').innerText = 'Android(打包中...)';
                }

            },
            error: function (err) {
                console.log(err);
            }
        }
    );
}

archiveBtn.addEventListener('click', function () {
    form.onsubmit = function () {
        return true
    };
});

archive_cancel.addEventListener('click', function () {
    archiveDialog.close();
    form.onsubmit = function () {
        return false
    };
});


// 上傳圖片<Dialog>
const showUploadButton = document.querySelectorAll('.showUploadDialog');
var cancelUploadButton = document.getElementById('cancelUpload');
var uploadDialog = document.getElementById('uploadDialog');
var submit = document.getElementById('sumitUpload');

showUploadButton.forEach(element => {
   element.setAttribute('style', 'background-color: #346beb; color: white; border: 0px; border-radius: 5px; height: 20px;');
   element.addEventListener('click', function onOpen(){
        submit.value = element.value;
        uploadDialog.show();
   });
});

cancelUploadButton.addEventListener('click', function (){
    uploadDialog.close();
});